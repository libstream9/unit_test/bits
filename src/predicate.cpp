#include <stream9/bits/predicate.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(predicate_)

    BOOST_AUTO_TEST_SUITE(all_of_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            int a = 0b000;
            int b = 0b000;

            BOOST_TEST(bit::all_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            int a = 0b101;
            int b = 0b000;

            BOOST_TEST(bit::all_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            int a = 0b000;
            int b = 0b100;

            BOOST_TEST(!bit::all_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            int a = 0b111;
            int b = 0b001;

            BOOST_TEST(bit::all_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            int a = 0b111;
            int b = 0b101;

            BOOST_TEST(bit::all_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            int a = 0b101;
            int b = 0b010;

            BOOST_TEST(!bit::all_of(a, b));
        }

    BOOST_AUTO_TEST_SUITE_END() // all_of_

    BOOST_AUTO_TEST_SUITE(any_of_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            int a = 0b000;
            int b = 0b000;

            BOOST_TEST(!bit::any_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            int a = 0b001;
            int b = 0b000;

            BOOST_TEST(!bit::any_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            int a = 0b000;
            int b = 0b001;

            BOOST_TEST(!bit::any_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            int a = 0b001;
            int b = 0b001;

            BOOST_TEST(bit::any_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            int a = 0b010;
            int b = 0b001;

            BOOST_TEST(!bit::any_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            int a = 0b001;
            int b = 0b101;

            BOOST_TEST(bit::any_of(a, b));
        }

    BOOST_AUTO_TEST_SUITE_END() // any_of_

    BOOST_AUTO_TEST_SUITE(none_of_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            int a = 0b000;
            int b = 0b000;

            BOOST_TEST(bit::none_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            int a = 0b000;
            int b = 0b001;

            BOOST_TEST(bit::none_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            int a = 0b010;
            int b = 0b000;

            BOOST_TEST(bit::none_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            int a = 0b010;
            int b = 0b001;

            BOOST_TEST(bit::none_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            int a = 0b010;
            int b = 0b011;

            BOOST_TEST(!bit::none_of(a, b));
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            int a = 0b011;
            int b = 0b011;

            BOOST_TEST(!bit::none_of(a, b));
        }

    BOOST_AUTO_TEST_SUITE_END() // none_of_

BOOST_AUTO_TEST_SUITE_END() // predicate_

} // namespace testing
