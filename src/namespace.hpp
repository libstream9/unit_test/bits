#ifndef STREAM9_BITS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_BITS_TEST_SRC_NAMESPACE_HPP

namespace stream9::bits {}

namespace testing {

namespace bit { using namespace stream9::bits; }

} // namespace testing

#endif // STREAM9_BITS_TEST_SRC_NAMESPACE_HPP
