#include <stream9/bits/modifier.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(modifier_)

    BOOST_AUTO_TEST_SUITE(set_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            int a = 0b000;
            int b = 0b000;

            bit::set(a, b);

            BOOST_TEST(a == 0b000);
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            int a = 0b001;
            int b = 0b000;

            bit::set(a, b);

            BOOST_TEST(a == 0b001);
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            int a = 0b000;
            int b = 0b010;

            bit::set(a, b);

            BOOST_TEST(a == 0b010);
        }

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            int a = 0b001;
            int b = 0b010;

            bit::set(a, b);

            BOOST_TEST(a == 0b011);
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            int a = 0b001;
            int b = 0b011;

            bit::set(a, b);

            BOOST_TEST(a == 0b011);
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            int a = 0b100;
            int b = 0b011;

            bit::set(a, b);

            BOOST_TEST(a == 0b111);
        }

        BOOST_AUTO_TEST_CASE(case_4_)
        {
            int a = 0b101;
            int b = 0b010;

            bit::set(a, b);

            BOOST_TEST(a == 0b111);
        }

    BOOST_AUTO_TEST_SUITE_END() // set_

    BOOST_AUTO_TEST_SUITE(reset_)

        BOOST_AUTO_TEST_CASE(empty_1_)
        {
            int a = 0b000;
            int b = 0b000;

            bit::reset(a, b);

            BOOST_TEST(a == 0b000);
        }

        BOOST_AUTO_TEST_CASE(empty_2_)
        {
            int a = 0b001;
            int b = 0b000;

            bit::reset(a, b);

            BOOST_TEST(a == 0b001);
        }

        BOOST_AUTO_TEST_CASE(empty_3_)
        {
            int a = 0b000;
            int b = 0b010;

            bit::reset(a, b);

            BOOST_TEST(a == 0b000);
        }

        BOOST_AUTO_TEST_CASE(case_1_)
        {
            int a = 0b101;
            int b = 0b001;

            bit::reset(a, b);

            BOOST_TEST(a == 0b100);
        }

        BOOST_AUTO_TEST_CASE(case_2_)
        {
            int a = 0b101;
            int b = 0b010;

            bit::reset(a, b);

            BOOST_TEST(a == 0b101);
        }

        BOOST_AUTO_TEST_CASE(case_3_)
        {
            int a = 0b111;
            int b = 0b101;

            bit::reset(a, b);

            BOOST_TEST(a == 0b010);
        }

    BOOST_AUTO_TEST_SUITE_END() // reset_

BOOST_AUTO_TEST_SUITE_END() // modifier_

} // namespace testing
