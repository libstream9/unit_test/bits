#include <stream9/bits/to_symbol.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

#define A1 1
#define A2 2
#define A3 4

namespace testing {

BOOST_AUTO_TEST_SUITE(ored_flags_to_symbol_)

#define ENTRY STREAM9_FLAG_ENTRY

    BOOST_AUTO_TEST_CASE(empty_)
    {
        int f = 0;
        auto s = bit::ored_flags_to_symbol(f, {
            ENTRY(A1),
            ENTRY(A2),
            ENTRY(A3),
        });

        BOOST_TEST(s == "unknown (0)");
    }

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        auto f = A1 | A3;
        auto s = bit::ored_flags_to_symbol(f, {
            ENTRY(A1),
            ENTRY(A2),
            ENTRY(A3),
        });

        BOOST_TEST(s == "A1 | A3");
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        auto f = A1 | A3 | 0b1000;
        auto s = bit::ored_flags_to_symbol(f, {
            ENTRY(A1),
            ENTRY(A2),
            ENTRY(A3),
        });

        BOOST_TEST(s == "A1 | A3 | unknown (8)");
    }

BOOST_AUTO_TEST_SUITE_END() // ored_flags_to_symbol_

} // namespace testing
